import { AlertController, NavController } from '@ionic/angular';
import { Component, Input, OnInit } from '@angular/core';
import { ModelModule} from '../../Models/ModelModule';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.scss'],

})

export class AgendaComponent implements OnInit {

  componentDatos: ModelModule.Datos;

  @Input()
  no: string;

  @Input()
  di: string;

  @Input()
  te: number;

  @Input()
  ce: number;

   @Input()
  co: string;

  constructor(public alerCtrl: AlertController) {  }

  async doAlert() {
    let alert = await this.alerCtrl.create({
      header: 'Informacion enviada!',
      message: 'Su informacion acaba de ser enviada!',
      buttons: ['Ok']
    });
    await alert.present();
  }

  ngOnInit() {

    this.componentDatos = <ModelModule.Datos>{};
    if(this.no !== undefined){
      this.componentDatos.Nombre = this.no;
    }
    

    if(this.di !== undefined){
      this.componentDatos.Direccion = this.di;
    }
    

    if(this.te !== undefined){
      this.componentDatos.Telefono = this.te;
    }
    

    if(this.ce !== undefined){
      this.componentDatos.Celular= this.ce;
    }

    if(this.co !== undefined){
      this.componentDatos.Correo = this.co;
    }
    

  }



}
