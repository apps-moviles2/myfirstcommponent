export module ModelModule{

    export interface Datos{
        Nombre: string;
        Direccion: string;
        Telefono: number;
        Celular: number;
        Correo: string;
    }
}

export default ModelModule;